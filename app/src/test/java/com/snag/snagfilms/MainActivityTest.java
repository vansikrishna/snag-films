package com.snag.snagfilms;

import com.snag.snagfilms.data.Film;
import com.snag.snagfilms.data.FilmsData;
import com.snag.snagfilms.data.ResponseData;
import com.snag.snagfilms.datamanager.DataModel;
import com.snag.snagfilms.interactor.ViewBinder;
import com.snag.snagfilms.presenter.MainPresenter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Matchers.any;


public class MainActivityTest {

    @Mock
    DataModel dataModel;
    @Mock
    ViewBinder viewBinder;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                return Schedulers.trampoline();
            }
        });
    }
    @Test
    public void test_loadFilms_is_success(){
        MainPresenter mainPresenter = new MainPresenter(dataModel);
        mainPresenter.attachView(viewBinder);
        ResponseData responseData = new ResponseData();
        responseData.films = new FilmsData();
        responseData.films.film = new ArrayList<Film>();
        Mockito.when(dataModel.loadFilms()).thenReturn(Observable.just(responseData));
        mainPresenter.loadFilms();
        Mockito.verify(viewBinder).renderData(any());
    }

    @After
    public void teardown(){
        dataModel = null;
        viewBinder = null;
    }
}
