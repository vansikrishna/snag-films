package com.snag.snagfilms.data;

import java.io.Serializable;

public class Film implements Serializable{

    public String id;
    public String title;
    public int durationMinutes, durationSeconds;
    public FilmImages images;

    public String getDuration(){
        return durationMinutes+":"+durationSeconds;
    }
}
