package com.snag.snagfilms;

import android.app.Application;

import com.snag.snagfilms.interactor.RetrofitService;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeApp extends Application{

    RetrofitService retrofitService;

    @Override
    public void onCreate() {
        super.onCreate();
        retrofitService = new Retrofit.Builder()
                .client(new OkHttpClient())
                .baseUrl("https://snagfilms.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build().create(RetrofitService.class);

    }

    public RetrofitService getRetrofitService() {
        return retrofitService;
    }
}
