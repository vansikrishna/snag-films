package com.snag.snagfilms.interactor;

public interface ViewBinder {
    void showProgress();
    void dismissProgress();
    void renderData(Object object);
    void showError(String error);
}
