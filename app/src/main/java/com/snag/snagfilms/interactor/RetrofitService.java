package com.snag.snagfilms.interactor;

import com.snag.snagfilms.data.ResponseData;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitService {

    @GET("apis/films.json")
    Observable<ResponseData> getFilms(@Query("limit") int limit);
}