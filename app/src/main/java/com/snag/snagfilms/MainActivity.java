package com.snag.snagfilms;


import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.snag.snagfilms.data.Film;
import com.snag.snagfilms.datamanager.DataModel;
import com.snag.snagfilms.interactor.ViewBinder;
import com.snag.snagfilms.presenter.BasePresenter;
import com.snag.snagfilms.presenter.MainPresenter;
import com.snag.snagfilms.presenter.PresenterLoader;
import com.snag.snagfilms.utils.DataAdapter;

import java.util.ArrayList;

public class MainActivity extends BaseActivity implements ViewBinder, LoaderManager.LoaderCallbacks<BasePresenter>{

    MainPresenter mainPresenter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recycler_view);
        getSupportLoaderManager().initLoader(1001, null, this);
    }

    @Override
    public Loader<BasePresenter> onCreateLoader(int id, Bundle args) {
        DataModel dataModel = new DataModel(((HomeApp) getApplication()).getRetrofitService());
        return new PresenterLoader(this, new MainPresenter(dataModel));
    }

    @Override
    public void onLoadFinished(Loader<BasePresenter> loader, BasePresenter presenter) {
        mainPresenter = (MainPresenter) presenter;
        mainPresenter.attachView(this);
        loadFilms();
    }

    @Override
    public void onLoaderReset(Loader<BasePresenter> loader) {

    }

    private void loadFilms(){
        if(mainPresenter != null){
            mainPresenter.loadFilms();
        }
    }

    @Override
    public void renderData(Object object) {
        Toast.makeText(this, "Load completed", Toast.LENGTH_LONG).show();
        setAdapterData((ArrayList<Film>) object);

    }

    private void setAdapterData(ArrayList<Film> films) {
        GridLayoutManager lLayout = new GridLayoutManager(this, 2);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(lLayout);
        DataAdapter adapter = new DataAdapter(this, films);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, "Sorry. Loading error - "+error, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mainPresenter != null){
            mainPresenter.detachView();
        }
    }
}
