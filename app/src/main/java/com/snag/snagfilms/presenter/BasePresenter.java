package com.snag.snagfilms.presenter;

import com.snag.snagfilms.interactor.ViewBinder;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BasePresenter {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    abstract void attachView(ViewBinder view);
    abstract void detachView();

    void addDisposableObserver(Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    public void dispose() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

}
