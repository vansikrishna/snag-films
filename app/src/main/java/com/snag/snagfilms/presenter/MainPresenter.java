package com.snag.snagfilms.presenter;


import com.snag.snagfilms.data.ResponseData;
import com.snag.snagfilms.datamanager.DataModel;
import com.snag.snagfilms.interactor.ViewBinder;

import io.reactivex.functions.Consumer;

public class MainPresenter extends BasePresenter{

    ViewBinder viewBinder;
    DataModel dataModel;

    public MainPresenter(DataModel dataModel){
        this.dataModel = dataModel;
    }

    @Override
    public void attachView(ViewBinder viewBinder) {
        this.viewBinder = viewBinder;
    }

    @Override
    public void detachView() {
        this.viewBinder = null;
        dispose();
    }

    public ViewBinder getViewBinder() {
        return viewBinder;
    }

    public void loadFilms(){
        viewBinder.showProgress();
        addDisposableObserver(dataModel.loadFilms()
        .subscribe(new Consumer<Object>() {
            @Override
            public void accept(Object object) throws Exception {
                viewBinder.dismissProgress();
                if(object instanceof ResponseData)
                    viewBinder.renderData(((ResponseData) object).films.film);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                viewBinder.dismissProgress();
                viewBinder.showError(throwable.toString());
            }
        }));
    }
}
