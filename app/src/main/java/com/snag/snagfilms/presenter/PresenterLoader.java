package com.snag.snagfilms.presenter;

import android.content.Context;
import android.support.v4.content.Loader;

public class PresenterLoader extends Loader<BasePresenter> {

    BasePresenter presenter;

    public PresenterLoader(Context context, MainPresenter mainPresenter) {
        super(context);
        presenter = mainPresenter;
    }

    @Override
    protected void onStartLoading() {
        deliverResult(presenter);
    }
}
