package com.snag.snagfilms.datamanager;

import com.snag.snagfilms.data.ResponseData;
import com.snag.snagfilms.interactor.RetrofitService;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class DataModel {

    RetrofitService retrofitService;

    public DataModel(RetrofitService retrofitService){
        this.retrofitService = retrofitService;
    }

    public Observable<ResponseData> loadFilms() {
        return retrofitService.getFilms(10)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
