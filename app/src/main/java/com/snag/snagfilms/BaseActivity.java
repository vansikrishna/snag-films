package com.snag.snagfilms;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity{

    ProgressDialog progressDialog;

    public void showProgress() {
        if(progressDialog == null){
            progressDialog = new ProgressDialog(this, 0);
            progressDialog.setMessage("Please Wait");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
        }
        progressDialog.show();
    }

    public void dismissProgress() {
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }
}
