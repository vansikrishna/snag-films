package com.snag.snagfilms.utils;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.snag.snagfilms.R;
import com.snag.snagfilms.data.Film;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Film> list;
    Context context;

    public class DataHolder extends RecyclerView.ViewHolder {
        TextView titleTextView, durationTextView;
        ImageView imageView;

        public DataHolder(View v) {
            super(v);
            titleTextView = v.findViewById(R.id.titleTextView);
            durationTextView = v.findViewById(R.id.durationTextView);
            imageView = v.findViewById(R.id.imageView);
        }
    }

    public DataAdapter(Context context, ArrayList<Film> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.data_item_row, parent, false);
        return new DataHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        DataHolder holder = (DataHolder) viewHolder;
        final Film entity = list.get(position);
        if(entity != null) {
            holder.titleTextView.setText(entity.title);
            holder.durationTextView.setText(entity.getDuration());
            Picasso.get()
                    .load((entity.images.image != null && entity.images.image.length > 0)? entity.images.image[0].src: "null")
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
